package main

import (
	"fmt"
	"os"
)

type Stream interface {
	read(n uint) []byte
	seek(ofs, whence int) bool
}

type StreamBuf struct {
	buf []byte
	size, ofs uint
}

type StreamFile struct {
	file *os.File
}

func streamBufInit(buf []byte, size uint) *StreamBuf {
	return &StreamBuf{buf: buf, size: size, ofs: 0}
}

func streamFileInit(file *os.File) *StreamFile {
	return &StreamFile{file: file}
}

func (s *StreamBuf) read(len uint) []byte {
	rem := s.size - s.ofs
	if len > rem {
		len = rem
	}
	return s.buf[s.ofs:(s.ofs + len)]
}

func (s *StreamBuf) seek(ofs, whence int) bool {
	switch whence {
	case 2:
		if ofs > 0 {
			return false
		}
		u := uint(-ofs)
		if u >= s.size {
			return false
		}
		s.ofs = s.size - 1 -u
	case 1:
		if ofs < 0 {
			return false
		}
		u := uint(ofs)
		if u >= s.size {
			return false
		}
		s.ofs = u
	case 0:
		if ofs < 0 {
			u := uint(-ofs)
			if u > s.ofs {
				return false
			}
			s.ofs -= u
		} else {
			u := s.ofs + uint(ofs)
			if u >= s.size {
				return false
			}
			s.ofs = u
		}
	default:
		return false
	}
	return true
}

func (s *StreamFile) read(len uint) []byte {
	b := make([]byte, len)
	s.file.Read(b)
	return b
}

func (s *StreamFile) seek(ofs, whence int) bool {
	_, err := s.file.Seek(int64(ofs), whence)
	return err == nil
}

var text []byte = []byte("The rain in Spain stays mainly in the plain")

func main() {
	var s Stream = streamBufInit(text, uint(len(text)))
	s.seek(4, 1)
	fmt.Println(string(s.read(4)))

	f, _ := os.Open("main.go")
	s = streamFileInit(f)
	s.seek(8, 1)
	fmt.Println(string(s.read(4)))
}
